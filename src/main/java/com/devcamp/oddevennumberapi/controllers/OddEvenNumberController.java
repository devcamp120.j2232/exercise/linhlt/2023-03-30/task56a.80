package com.devcamp.oddevennumberapi.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
@CrossOrigin
public class OddEvenNumberController {
    @GetMapping("/checknumber")
    public String checkNumberApi(@RequestParam(required = true, name = "number") int number){
        if (number % 2 == 0){
            return "This is even number";
        }
        else return "This is odd number";
    }
}
